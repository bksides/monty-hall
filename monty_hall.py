import random
import argparse

def main():
    # Parse the command line arguments...
    parser = argparse.ArgumentParser(description="A Monty Hall problem simulator")
    parser.add_argument("--doors", "-d", default=3, type=int, help="The number of doors in the game")
    parser.add_argument("--runs", "-r", default=1, type=int, help="The total number of games to simulate")
    parser.add_argument("--verbose", "-v", action="store_true", help="Print the details of each run")
    parser.add_argument("--monty-fall", "-f", action="store_true",
                        help="Simulate the \"Monty Fall\" problem, in which Monty opens a door at complete random,"
                             "potentially revealing the car and spoiling the game")
    parser.add_argument("--strategy", "-s", choices=["interactive", "optimal", "always-switch", "always-keep",
                                                     "random"],
                        default="interactive", help="The strategy the contestant will follow when picking doors")
    args = parser.parse_args()

    # interpret the strategy arguments
    strategy_map = {"interactive": interactive, "optimal": optimal, "always-switch": always_switch,
                    "always-keep": always_keep, "random": random_choice}
    strategy = strategy_map[args.strategy]
    monty_strategy = monty_fall if args.monty_fall else conventional

    total_wins = 0
    total_runs = 0
    # Do the runs!
    while total_runs < args.runs:
        if args.verbose:
            print("\nROUND", total_runs+1)
        result = monty_hall(args.doors, args.verbose, strategy, monty_strategy)
        if result:
            total_wins += 1
        if result is not None:
            total_runs += 1
    print("TOTAL WINS:", total_wins, "/", args.runs)

# CONTESTANT STRATEGIES

# The optimal strategy: pick a door at random, stick with it till the last minute, then switch
def optimal(contestant_door, pickable_doors):
    if contestant_door is None:
        return random.choice(pickable_doors)
    elif len(pickable_doors) > 2:
        return contestant_door
    return pickable_doors[1 if contestant_door == pickable_doors[0] else 0]

# Switch doors every turn
def always_switch(contestant_door, pickable_doors):
    return random.choice([door for door in pickable_doors if door != contestant_door])

# Always keep the initial choice
def always_keep(contestant_door, pickable_doors):
    if contestant_door is None:
        return random.choice(pickable_doors)
    return contestant_door

# Interactive - the user plays the role of the contestant
def interactive(_, pickable_doors):
    print("Please pick one of the following doors:")
    for door in pickable_doors:
        print("-", door)
    picked_door = int(input())
    while picked_door not in pickable_doors:
        print("Invalid choice! Try again.")
        picked_door = int(input())
    return picked_door

def random_choice(_, pickable_doors):
    return random.choice(pickable_doors)

# MONTY STRATEGIES

# The "Monty Fall" problem - Monty opens a door at complete random, as if he tripped and opened one by accident
# This means he could accidentally reveal the car, spoiling the game!
def monty_fall(_, revealable_doors):
    return random.choice(revealable_doors)

# The conventional Monty Hall format - Monty never opens the door containing the car
def conventional(car_door, revealable_doors):
    options = [door for door in revealable_doors if door != car_door]
    if options:
        return random.choice(options)
    return car_door

# Simulates a single round of monty-hall with the given number of doors and the given strategies
def monty_hall(num_doors, verbose=False, contestant_strategy=optimal, monty_strategy=conventional):
    # First, we pick a random door to hide the car behind
    car_door = random.randrange(num_doors)
    if verbose:
        print("The car is behind door", car_door)
    # now the contestant and Monty will take turns choosing and opening doors, respectively.  We will keep track of the
    # unopened doors that they can choose from.
    unopened_doors = [door for door in range(num_doors)]
    contestant_door = None
    revealed_door = None
    while len(unopened_doors) > 1:  # as long as there are more doors to open, keep going.
        # at the beginning of each turn we check to see if Monty revealed the car last time
        # if he did, the game is botched!
        if revealed_door == car_door:
            if verbose or contestant_strategy == interactive:
                print("Monty revealed the car before the game was up!  Discarding this run...")
            return None
        # first, the contestant picks a door
        contestant_door = contestant_strategy(contestant_door, unopened_doors)
        if verbose:
            print("The contestant picked door", contestant_door)
        # Now Monty opens some door other than the one the contestant picked
        revealed_door = monty_strategy(car_door, [door for door in unopened_doors if door != contestant_door])
        # We remove the opened door from our master list
        unopened_doors.remove(revealed_door)
        if verbose or contestant_strategy == interactive:
            print("Monty revealed a", "car" if revealed_door == car_door else "goat", "behind door", revealed_door)
    # Once all is said and done, if the contestant has selected the car door, they win!
    if contestant_door == car_door:
        if verbose or contestant_strategy == interactive:
            print("The contestant won!")
        return True
    else:
        if verbose or contestant_strategy == interactive:
            print("The contestant lost...")
        return False

if __name__ == "__main__":
    main()