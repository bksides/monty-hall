# Monty Hall Simulator

## Usage

`python3 monty_hall.py [-vf] [-d <number of doors>] [-r <number of runs>] [-s <strategy>]`

## Options

- `--verbose`, `-v`: Print the details of each run
- `--monty-fall`, `-f`: Simulate the "Monty Fall" problem, in which Monty opens a door at complete
    random, potentially revealing the car and spoiling the game.
- `--doors`, `-d`: The number of doors in each game.  Defaults to 3.
- `--runs`, `-r`: The total number of games to simulate.  Defaults to 1.
- `--strategy`, `-s`: The strategy to use when picking doors.  Possible strategies are:
    - `interactive` (default): the user plays the role of the contestant
    - `optimal`: the contestant sticks with a door till the end, and switches at the last minute.
    - `always-switch`: the contestant will pick a different door each turn
    - `always-keep`: the contestant will stick with the same door for the entire game
    - `random`: the contestant will pick a random door each turn.  Unlike `always-switch`, it's possible that the
        contestant will pick the same door multiple turns in a row.
      
## The Premise

The Monty Hall problem is initially confusing to many people because it violates a very strong intuition about
probability.  The premise of the problem is this: you are a contestant on the game show *Let's Make a Deal*, hosted by
the titular Monty Hall.  Before you are three closed doors; you don't know what's behind any one of them.  Monty informs
you that one of the doors conceals a new car, which you can win if you play your cards right.  The other two, however,
each contain a goat, which you don't have any particular interest in winning.  You don't know which is which.

Monty allows you to pick a door, which you do.  Monty then opens one of the other two doors to reveal a goat.  Monty
knows which door the car is behind, so to keep the game interesting, he never opens the door concealing the car.  He
then asks you if you'd like to keep what's behind the door you originally picked, or *switch*, and take the prize behind
the remaining closed door.

Most peoples' intuition is that it can't possibly matter which of the two doors you picked.  The two doors were equally
likely to conceal a goat in the first place, right?  Why should it matter what was behind the *third* door?  After all,
there was never any doubt that Monty would reveal a goat as opposed to the car.  So what information are you really
gaining?

The reality is that the two doors are not on equal footing anymore.  You are significantly better off switching - if you
do, you have a 2/3 chance of winning, vs a 1/3 chance if you keep your original choice.

## Demonstration

The ultimate purpose of this program is to empirically demonstrate this unintuitive fact.  Many people believe so
strongly that the solution to the Monty Hall problem *can't be right* that they don't believe it, no matter what
reasoning is presented, until they see it with their own eyes, so before we get into the reasoning let's run through the
program.  I am literally running the program *as I type this*, with no selection process.  Feel free to download the
program and run it yourself if you don't believe me.

Let's start by getting a feel for the game.  I'm going to play a few interactive rounds and copy the results here.
I'll do 10 rounds total:
```bash
python3 monty_hall.py --runs 10
```
Once I start the program, Monty immediately prompts me to pick a door:
```
Please pick one of the following doors:
- 0
- 1
- 2
```
I'll pick the middle door, door 1.
```
1  
Monty revealed a goat behind door 2
Please pick one of the following doors:
- 0
- 1
```
After I pick door 1, Monty reveals a goat behind door 2, and asks me to pick one of the remaining doors.  I can either
stick with my original choice, or switch.  I'll follow my own advice and switch to door 0:
```
0
Monty revealed a goat behind door 1
The contestant won!
```
Hey, look, I won!  Again, no selection process here.  I could have lost, which would have been a bit embarrassing.  But
of course a single round is not enough to show anything either way.  Here are the results of
the next 9 rounds:
```
Please pick one of the following doors:
- 0
- 1
- 2
2
Monty revealed a goat behind door 1
Please pick one of the following doors:
- 0
- 2
0
Monty revealed a goat behind door 2
The contestant won!
Please pick one of the following doors:
- 0
- 1
- 2
0
Monty revealed a goat behind door 1
Please pick one of the following doors:
- 0
- 2
2
Monty revealed a goat behind door 0
The contestant won!
Please pick one of the following doors:
- 0
- 1
- 2
2
Monty revealed a goat behind door 0
Please pick one of the following doors:
- 1
- 2
1
Monty revealed a goat behind door 2
The contestant won!
Please pick one of the following doors:
- 0
- 1
- 2
1
Monty revealed a goat behind door 0
Please pick one of the following doors:
- 1
- 2
2
Monty revealed a car behind door 1
The contestant lost...
Please pick one of the following doors:
- 0
- 1
- 2
2
Monty revealed a goat behind door 1
Please pick one of the following doors:
- 0
- 2
0
Monty revealed a car behind door 2
The contestant lost...
Please pick one of the following doors:
- 0
- 1
- 2
0
Monty revealed a goat behind door 2
Please pick one of the following doors:
- 0
- 1
1
Monty revealed a goat behind door 0
The contestant won!
Please pick one of the following doors:
- 0
- 1
- 2
1
Monty revealed a goat behind door 0
Please pick one of the following doors:
- 1
- 2
2
Monty revealed a goat behind door 1
The contestant won!
Please pick one of the following doors:
- 0
- 1
- 2
0
Monty revealed a goat behind door 2
Please pick one of the following doors:
- 0
- 1
1
Monty revealed a car behind door 0
The contestant lost...
Please pick one of the following doors:
- 0
- 1
- 2
2
Monty revealed a goat behind door 1
Please pick one of the following doors:
- 0
- 2
0
Monty revealed a goat behind door 2
The contestant won!
TOTAL WINS: 7 / 10
```
You can walk through each round if you'd like, but the important thing is the final line:
I ended up winning 7/10 times with the switching strategy.  I couldn't have asked for a
better set of runs for this simulation, since this is the nearest whole number to the ideal
statistical win ratio, which would have been 2/3 of the total or 6.666....

However, this is kind of a slow means of testing, and 7 out of 10 could easily be a fluke.
So hopefully now that I've demonstrated that my simulator is faithful to the original form
of the game, we can automate the guessing process, run many simulations and see how it turns
out.

We can do this by specifying a guessing strategy and ramping up the number of runs:
```bash
python3 monty_hall.py --runs 1000 --strategy optimal
```
We're using the strategy called "optimal", which for this formulation of the game just means
switching when Monty gives you the chance.  We run 1000 games; I won't take up a bunch of space
by pasting every result of every game here, but I will paste the final results:
```
TOTAL WINS: 665 / 1000
```
Another really lucky set of runs - we're again extremely close to the ideal value, which
would be 666.6666....

Hopefully now you're convinced, at least empirically, that switching is the optimal strategy.  If the true underlying
probability of winning by switching were 50%, winning 665/1000 runs would be an *enormous*
fluke.  But it happens very reliably; I personally have never seen a fluke go the other way,
producing roughly 500 wins out of 1000.  So there's very good reason to believe that this
figure reflects the true underlying probability.  Hell, let's do a million simulations just
to be extra, extra sure:
```
TOTAL WINS: 666369 / 1000000
```
At this point, I shouldn't even need to bother convincing you I'm not applying a selection
process, because if the true probability were 50%, I would basically *never* see a fluke so
big that it produces an extra 166,369 wins.  So now that we're thoroughly convinced, let's
try to align our intuition with the empirical reality.

## Explanation

There have been a million attempts to elucidate exactly *why* switching is the best strategy, in a way that makes sense
to people.  There's kind of an "old faithful" explanation that most people default to, and it goes a bit like this:

First, you realize that on the first choice, you have a 1/3 chance of selecting the door with the car behind it.
In this case, it doesn't matter which door Monty opens; when you switch, you'll get a goat, and lose.

Similarly, you have a 2/3 chance of selecting a *goat* on your first turn.  In this case, Monty's hand is forced:
there's only one remaining door that Monty can open, and the one remaining contains the car.  So if you
switch, you win.

We can make this effect even more pronounced if we imagine that there are more doors - say there are 100.  The player
selects an initial door, after which point Monty opens *all but one* of the remaining doors to reveal goats behind them.
There are two possibilities: either you selected the door concealing the car on your first try, or the one remaining
door contains the car.  Of course, there's only a 1% chance that you picked the car the first time, so you have a 99%
chance of winning if you switch.  Just to see this in action, we can run our simulator with 100 doors and see how often
we come out on top if we switch:
```
python3 monty_hall.py --doors 100 --runs 100 --strategy optimal
TOTAL WINS: 99 / 100
```
And there we go - the exact 99% success rate we expect.

This is a good explanation.  It clarifies exactly where the 2/3 chance of winning comes from: it comes from the fact
that you have a 2/3 chance of selecting a goat in the first place, and if that happens, switching after the other goat
is revealed will get you a car.  But it's not entirely complete... To see why, let's imagine changing the problem up a
bit.

## The "Monty Fall" Problem

Let's imagine that we're playing a Monty Hall round, as usual, but as Monty goes to open one of the doors, he falls over
and *accidentally* opens one of the remaining doors, revealing a goat.  From your perspective, as a contestant, this
might appear to be an exactly identical situation.  You had a 1/3 chance of selecting the car in the first place, and
one of the other doors is now eliminated, so there should be a 2/3 chance that the car is behind the remaining door.
The logic we walked through before seems like it should apply equally well to this scenario.  Unfortunately, as mind-
bending as the original Monty Hall problem is, it gets worse: in this scenario, switching and staying each give you a
50% chance of winning.

This simulator is once again equipped to prove to you that this is the case.  We can use the `--monty-fall` option
to force Monty to open one of the two unchosen doors at random.  We can then discard the results of the runs in which
he reveals a car, so as to account only for the scenario in which he randomly reveals a goat.
```
python3 monty_hall.py --runs 1000 --strategy optimal --monty-fall
TOTAL WINS: 509 / 1000
```
This time, we only win a little more than 50% of the games.  So what's happening here?

I dropped a hint about this when I was describing the scenario.  The situation is not exactly identical, even though
it may look like it from your perspective, and we can see that by realizing that in order to arrive at these numbers
we had to *discard* a number of runs in which Monty revealed a car, rather than a goat.  Some of our simulations aren't
counting towards the final tally!

To see what I mean, here is an example in which I play the role of the contestant:
```
python3 monty_hall.py --runs 1000 --strategy interactive --monty-fall
Please pick one of the following doors:
- 0
- 1
- 2
0
Monty revealed a goat behind door 2
Please pick one of the following doors:
- 0
- 1
1
Monty revealed a goat behind door 0
The contestant won!
```
Like we said, this looks pretty much identical from our perspective to the situation we had before.  This time, I
happen to have won.  But things *could* have gone another way...
```
Please pick one of the following doors:
- 0
- 1
- 2
2
Monty revealed a car behind door 1
Monty revealed the car before the game was up!  Discarding this run...
```
If Monty accidentally reveals the car, we have violated the premise of the problem.  So we have to discard that run.

The important thing to notice is that Monty can only reveal a car if you didn't *pick* the door hiding the car in the
first place - remember, the premise is that he randomly opens one of the doors you *didn't* pick.  Therefore, a given
run is more likely to end up being discarded (in fact, it can *only* be discarded) if you start off with a door that
hides a goat.

But the logic of the original scenario relies on it being *more likely* that you originally picked a goat.  It turns out
that these two things - the probability that you originally pick a "goat door" vs. the probability that the scanario will
end up being discarded - exactly cancel out, leaving you with a 50% chance of having selected a door concealing a goat
in a scenario where Monty reveals a goat behind one of the remaining doors.

This explanation might sound confusing, and even if it's not confusing, it might not be very satisfying.  Why should
these exactly cancel out?  And even if they do, it just doesn't seem to make sense that two seemingly identical
situations - should involve totally different probabilities, regardless of how we got there.  However, I've
intentionally omitted a vital concept from that explanation so that we can take it on in a more holistic way.  That
concept is *information*.

## The Informational Approach: Bayesian Reasoning

There is a very fundamental question that lies at the heart of the Monty Hall problem and its variants, and that
question is this: how does new information cause us to update our view of the world?

In each case, Monty opens a door to reveal a goat.  This is a new, identical piece of information in both cases.
So why then do we come to *different* conclusions?

Human brains are, at the end of the day, rather simple reasoning machines.  We tend to process information in a
relatively straightforward way, taking in very little context, and attempting to draw conclusions from the new
information alone.  However, context is often important.  Let's take an example.

Let's imagine there exists a terrible but rare illness.  Its symptoms are non-specific, so the only way to detect it
is by means of a test.  The test has a 0.1% false-negative rate, meaning that for 1000 people who don't have the
illness, only one will test positive.  Imagine an individual tests positive for the illness.  Normal human reasoning
tells us that, since the test rarely gives false positives, that the individual most likely has the illness.  Right?

Not so fast.  There's more context to consider.  Recall that I said the illness itself is very rare - depending on
how rare it is, it could be *more* likely that the individual produced a false positive than that they actually have
the illness.  So we have *new information* - the test result - that we have to combine with our *previous knowledge* -
the overall rarity of the illness - to somehow come up with an overall probability that the individual actually
has the illness.

This is the essence of *Bayesian reasoning*.  The name comes from Thomas Bayes, who derived a formula for calculating
probabilities in exactly these kinds of circumstances.  The formula is called *Bayes' Theorem*, and here it is, in all
its glory:

<img src="https://render.githubusercontent.com/render/math?math=P(A|B)=\frac{P(B|A)P(A)}{P(B)}">